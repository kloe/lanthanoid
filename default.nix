let
	pkgs = import <nixpkgs> {};
in
	pkgs.stdenv.mkDerivation {
		name = "lanthanoid";
		src = ./.;
		buildInputs = [ pkgs.mercury ];
	}
