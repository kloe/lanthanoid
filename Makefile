SOURCES=$(wildcard src/*.m)

all: src/lanthanoid

.PHONY: install
install: src/lanthanoid
	cp $^ ${out}

src/lanthanoid: ${SOURCES}
	(cd src && mmc --make lanthanoid)
