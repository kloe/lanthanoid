%------------------------------------------------------------------------------%

:- module la_player.

%------------------------------------------------------------------------------%

:- interface.

:- type id.

% TODO: Remove this when implementing players properly. This is currently only
% TODO: used for debugging.
:- func dummy = id.

%------------------------------------------------------------------------------%

:- implementation.

:- type id == int.

dummy = 1.

%------------------------------------------------------------------------------%
