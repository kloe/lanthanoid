%------------------------------------------------------------------------------%

:- module la_look.

%------------------------------------------------------------------------------%

:- interface.
:- use_module bool.
:- use_module la_item.
:- use_module la_map.
:- use_module maybe.

	% Describe an area to the player.
:- pred look(la_map.map, {int, int}, string).
:- mode look(in, in, out) is semidet.

	% Describe the environment of an area to the player.
:- pred look_environment(la_map.environment, string).
:- mode look_environment(in, out) is det.

	% The description is just displayed as-is.
:- pred look_description(string, string).
:- mode look_description(in, out) is det.

	% Describe the mine of an area to the player.
:- pred look_mine(maybe.maybe(la_item.item), string).
:- mode look_mine(in, out) is det.

	% Describe the paths out of an area to the player.
:- pred look_paths(la_map.direction_wise(bool.bool), string).
:- mode look_paths(in, out) is det.

%------------------------------------------------------------------------------%

:- implementation.
:- import_module string. % ++

	% If a condition is true, return the given string, otherwise return the
	% empty string.
:- pred cond(bool.bool, string, string).
:- mode cond(in, in, out) is det.

look(Map, Pos, Msg) :-
	Area = Map ^ la_map.area(Pos),
	look_environment(Area ^ la_map.environment, MEnv),
	look_description(Area ^ la_map.description, MDesc),
	look_paths(Area ^ la_map.paths, MPaths),
	look_mine(Area ^ la_map.mine, MMine),
	Msg = MEnv ++ MDesc ++ MPaths ++ MMine.

look_environment(la_map.building, "You are inside a building.\n").
look_environment(la_map.outside,  "You are outside.\n").
look_environment(la_map.cave,     "You are inside a cave.\n").

look_description(Msg, Msg ++ "\n").

look_paths(Paths, Msg) :-
	cond(Paths ^ la_map.north, "There is a path to the north.\n", MN),
	cond(Paths ^ la_map.south, "There is a path to the south.\n", MS),
	cond(Paths ^ la_map.west,  "There is a path to the west.\n",  MW),
	cond(Paths ^ la_map.east,  "There is a path to the east.\n",  ME),
	Msg = MN ++ MS ++ MW ++ ME.

look_mine(maybe.no, "").
look_mine(maybe.yes(Ore), Msg) :-
	la_item.describe(Ore, MOre),
	Msg = "There is a mine full of " ++ MOre ++ ".\n".

cond(Cond, InStr, OutStr) :-
	( if Cond = bool.yes
	then OutStr = InStr
	else OutStr = "" ).

%------------------------------------------------------------------------------%
