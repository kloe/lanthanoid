%------------------------------------------------------------------------------%

:- module la_map.

%------------------------------------------------------------------------------%

:- interface.
:- use_module bool.
:- use_module la_item.
:- use_module list.
:- use_module maybe.
:- import_module int. % + -

	% A map is a collection of areas. It maps area coordinates to areas,
	% such that areas can be found in (near-)constant time. Maps are not
	% to be modified during gameplay.
:- type map.

	% An area record describes what an area looks like, what areas can be
	% accessed from it, and what artifacts it features.
:- type area ---> area(
		% What is the environment? The environment may not just be
		% aesthetic; it may have an effect on the gameplay.
	environment :: environment,

		% A general description of the area, to be displayed to the player.
	description :: string,

		% In which directions can the player travel to adjacent areas?
	paths :: direction_wise(bool.bool),

		% What can be mined here, if anything?
	mine :: maybe.maybe(la_item.item)
).

	% The environment of an area.
:- type environment
	--->	building
	;	outside
	;	cave.

	% One of the directions in which one may travel.
:- type direction
	--->	north
	;	south
	;	west
	;	east.

	% A direction-wise record maps each possible direction to a value.
:- type direction_wise(T) ---> direction_wise(
	north	:: T,
	south	:: T,
	west	:: T,
	east	:: T
).

	% Create a map given a list of areas.
:- pred make(list.list({{int, int}, area}), map).
:- mode make(in, out) is det.

	% Given a map, look up an area.
:- func map ^ area({int, int}) = area.
:- mode in ^ area(in) = out is semidet.

	% Given a direction, adjust a coordinate by moving in that direction by
	% a unit distance.
:- pred adjust_position(direction, {int, int}, {int, int}).
:- mode adjust_position(in, in, out) is det.

	% Given a direction-wise record and a direction, return the appropriate
	% value from the record.
:- func direction_wise(T) ^ direction(direction) = T.
:- mode in ^ direction(in) = out is det.

	% Given a direction, travel in that direction if possible. It is
	% possible if the area says there is a path, and the target area
	% exists.
:- pred travel(direction, map, {int, int}, {int, int}).
:- mode travel(in, in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- implementation.
:- use_module map.

:- type map == map.map({int, int}, area).

make(Areas, Map) :-
	Map = list.foldl(
		func({Pos, Area}, Acc) = map.set(Acc, Pos, Area),
		Areas,
		map.init
	).

Map ^ area(Pos) = Area :-
	map.search(Map, Pos, Area).

adjust_position(north, {FromX, FromY}, {FromX, FromY - 1}).
adjust_position(south, {FromX, FromY}, {FromX, FromY + 1}).
adjust_position(west,  {FromX, FromY}, {FromX - 1, FromY}).
adjust_position(east,  {FromX, FromY}, {FromX + 1, FromY}).

DW ^ direction(north) = DW ^ north.
DW ^ direction(south) = DW ^ south.
DW ^ direction(west)  = DW ^ west.
DW ^ direction(east)  = DW ^ east.

travel(Dir, Map, From, To) :-
	FromArea = map.search(Map, From),
	FromArea ^ paths ^ direction(Dir) = bool.yes,
	adjust_position(Dir, From, To),
	map.contains(Map, To).

%------------------------------------------------------------------------------%
