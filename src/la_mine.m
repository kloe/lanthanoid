%------------------------------------------------------------------------------%

:- module la_mine.

%------------------------------------------------------------------------------%

:- interface.
:- use_module la_player.
:- use_module la_state.

	% Mine the ore in the current area. If there is no ore, this predicate
	% fails.
:- pred mine(la_player.id, la_state.state, la_state.state).
:- mode mine(in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- implementation.
:- use_module la_item.
:- use_module la_map.
:- use_module maybe.

mine(Player, !State) :-
	la_state.map(!.State, Map),
	la_state.player_area(Player, !.State, Area),
	maybe.yes(Ore) = Map ^ la_map.area(Area) ^ la_map.mine,

	la_state.player_inventory(Player, !.State, OldInv),
	la_item.insert(Ore, OldInv, NewInv),
	la_state.player_inventory(Player, NewInv, !State).

%------------------------------------------------------------------------------%
