%------------------------------------------------------------------------------%

:- module la_item.

%------------------------------------------------------------------------------%

:- interface.
:- use_module list.

	% Items.
:- type item
	--->	food_apple
	;	food_apple_strudel
	;	food_flour
	;	ore_lithium.

	% All items, as a predicate.
:- pred all(item).
:- mode all(in) is det.
:- mode all(out) is multi.

	% All items, as a list.
:- pred all_list(list.list(item)).
:- mode all_list(out) is det.

	% Describe the item to the player.
:- pred describe(item, string).
:- mode describe(in, out) is det.
:- mode describe(out, in) is semidet.

% -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - %

	% An inventory is a mapping from each item to a count.
:- type inventory.

	% The inventory with a zero count for each item.
:- pred empty(inventory).
:- mode empty(out) is det.

	% Create an inventory from a list of items and counts.
:- func make(list.list({item, int})) = inventory.
:- pred make(list.list({item, int}), inventory).
:- mode make(in, out) is det.

	% Insert an item into an inventory.
:- pred insert(item, inventory, inventory).
:- mode insert(in, in, out) is det.

	% How many items of this type are in this inventory?
:- pred count(item, inventory, int).
:- mode count(in, in, out) is det.

	% Describe the inventory to the player.
:- pred describe_inventory(inventory, string).
:- mode describe_inventory(in, out) is det.

%------------------------------------------------------------------------------%

:- implementation.
:- use_module map.
:- use_module solutions.
:- import_module int. % +
:- import_module string. % ++

all(food_apple).
all(food_apple_strudel).
all(food_flour).
all(ore_lithium).

all_list(Items) :-
	Lambda = (pred(I :: out) is multi :- all(I)),
	solutions.solutions(Lambda, Items).

describe(food_apple, "apple").
describe(food_apple_strudel, "apple strudel").
describe(food_flour, "flour").
describe(ore_lithium, "lithium ore").

% -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - %

:- type inventory == map.map(item, int).

empty(map.init).

make(Items) = Inv :-
	make(Items, Inv).
make(Items, Inv) :-
	Lambda = (
		pred({I, N} :: in, AccIn :: in, AccOut :: out) is det :-
			map.set(I, N, AccIn, AccOut)
	),
	list.foldl(Lambda, Items, map.init, Inv).

insert(Item, !Inv) :-
	count(Item, !.Inv, Count),
	map.set(Item, Count + 1, !Inv).

count(Item, Inv, Count) :-
	( if map.search(Inv, Item, OkCount)
	then Count = OkCount
	else Count = 0 ).

describe_inventory(Inv, Msg) :-
	Lambda = (
		pred(I :: in, N :: in, AccIn :: in, AccOut :: out) is det :-
			( if N = 0 then
				AccOut = AccIn
			else
				describe(I, Desc),
				NStr = string.int_to_string_thousands(N),
				AccOut = AccIn ++ NStr ++ " × " ++ Desc ++ "\n"
			)
	),
	map.foldl(Lambda, Inv, "", Msg).

%------------------------------------------------------------------------------%
