%------------------------------------------------------------------------------%

:- module lanthanoid.

%------------------------------------------------------------------------------%

:- interface.
:- use_module io.

:- pred main(io.io, io.io).
:- mode main(di, uo) is det.

%------------------------------------------------------------------------------%

:- implementation.
:- use_module bool.
:- use_module la_command.
:- use_module la_item.
:- use_module la_look.
:- use_module la_map.
:- use_module la_mine.
:- use_module la_player.
:- use_module la_state.
:- use_module la_travel.
:- use_module maybe.
:- use_module string.
:- import_module list. % [] [|]

	% Accept a command, parse them, interpret them, and print the result,
	% and loop, doing this forever.
:- pred loop(la_state.state, io.io, io.io).
:- mode loop(in, di, uo) is det.

	% After having read a command string, this will parse it and invoke
	% on_command on success.
:- pred on_command_string(
	string,
	la_state.state, la_state.state,
	io.io, io.io
).
:- mode on_command_string(in, in, out, di, uo) is det.

	% Interpret a command, print the message, and handle any errors.
:- pred on_command(
	la_command.command,
	la_state.state, la_state.state,
	io.io, io.io
).
:- mode on_command(in, in, out, di, uo) is det.

	% Update the state according to a command.
:- pred interpret(la_command.command, la_state.state, la_state.state, string).
:- mode interpret(in, in, out, out) is semidet.

	% Create a map for testing.
:- pred make_map(la_map.map).
:- mode make_map(out) is det.

main(!IO) :-
	make_map(Map),
	la_state.make(
		Map,
		[{la_player.dummy, {0, 0}}],
		[
			{
				la_player.dummy,
				la_item.make([
					{la_item.food_apple, 1},
					{la_item.food_flour, 3}
				])
			}
		],
		State
	),
	loop(State, !IO).

% -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - %

loop(OldState, !IO) :-
	io.format("> ", [], !IO),
	io.read_line_as_string(CommandRes, !IO),
	( CommandRes = io.ok(CommandStr),
		on_command_string(CommandStr, OldState, NewState, !IO),
		loop(NewState, !IO)

	; CommandRes = io.eof,
		io.format("\nGoodbye.\n", [], !IO)

	; CommandRes = io.error(Error),
		io.error_message(Error, ErrorMsg),
		io.format("Oops: %s\n", [string.s(ErrorMsg)], !IO) ).

on_command_string(CommandStr, !State, !IO) :-
	( if la_command.parse(CommandStr, Command)
	then on_command(Command, !State, !IO)
	else io.format("I do not understand that.\n", [], !IO) ).

on_command(Command, !State, !IO) :-
	( if interpret(Command, !.State, NewState, Message) then
		io.format("%s", [string.s(Message)], !IO),
		!:State = NewState
	else
		io.format("You cannot do that.\n", [], !IO)
	).

% -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - %

interpret(la_command.look, !State, Message) :-
	la_state.map(!.State, Map),
	la_state.player_area(la_player.dummy, !.State, Area),
	la_look.look(Map, Area, Message).

interpret(la_command.inventory, !State, Message) :-
	la_state.player_inventory(la_player.dummy, !.State, Inv),
	la_item.describe_inventory(Inv, Message).

interpret(la_command.travel(Dir), !State, Message) :-
	la_travel.travel(Dir, la_player.dummy, !State),
	interpret(la_command.look, !State, Message).

interpret(la_command.mine, !State, Message) :-
	la_mine.mine(la_player.dummy, !State),
	interpret(la_command.inventory, !State, Message).

% -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - %

make_map(Map) :-
	Paths = la_map.direction_wise(bool.no, bool.no, bool.no, bool.no),
	la_map.make([
		{
			{0, 0},
			la_map.area(
				la_map.outside,
				"You are surrounded by mountains.",
				Paths ^ la_map.east := bool.yes,
				maybe.no
			)
		},
		{
			{1, 0},
			la_map.area(
				la_map.cave,
				"It is very dark and you cannot see much.",
				Paths ^ la_map.south := bool.yes,
				maybe.yes(la_item.ore_lithium)
			)
		},
		{
			{0, 1},
			la_map.area(
				la_map.building,
				"You are in a large room.",
				Paths ^ la_map.north := bool.yes,
				maybe.no
			)
		},
		{
			{1, 1},
			la_map.area(
				la_map.cave,
				"The cave is lit through a hole in the ceiling.",
				Paths ^ la_map.west := bool.yes,
				maybe.no
			)
		}
	], Map).

%------------------------------------------------------------------------------%
