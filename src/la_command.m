%------------------------------------------------------------------------------%

:- module la_command.

%------------------------------------------------------------------------------%

:- interface.
:- use_module la_map.

	% Commands are input by users.
:- type command
	--->	look
	;	inventory
	;	travel(la_map.direction)
	;	mine.

	% Parse a command.
:- pred parse(string, command).
:- mode parse(in, out) is semidet.

%------------------------------------------------------------------------------%

:- implementation.
:- use_module string.

	% Like parse, but only for cleaned-up input.
:- pred parse_clean(string, command).
:- mode parse_clean(in, out) is semidet.

parse(Raw, Command) :-
	Clean = string.strip(Raw),
	parse_clean(Clean, Command).

parse_clean("l", look).
parse_clean("i", inventory).
parse_clean("n", travel(la_map.north)).
parse_clean("s", travel(la_map.south)).
parse_clean("w", travel(la_map.west)).
parse_clean("e", travel(la_map.east)).
parse_clean("m", mine).

%------------------------------------------------------------------------------%
