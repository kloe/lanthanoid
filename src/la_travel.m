%------------------------------------------------------------------------------%

:- module la_travel.

%------------------------------------------------------------------------------%

:- interface.
:- use_module la_map.
:- use_module la_player.
:- use_module la_state.

	% Given a direction, move the player in that direction. If the player
	% does not exist, or cannot travel in that direction, this predicate
	% will fail.
:- pred travel(la_map.direction, la_player.id, la_state.state, la_state.state).
:- mode travel(in, in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- implementation.

travel(Dir, Player, !State) :-
	la_state.map(!.State, Map),
	la_state.player_area(Player, !.State, OldArea),
	la_map.travel(Dir, Map, OldArea, NewArea),
	la_state.relocate_player(Player, NewArea, !State).

%------------------------------------------------------------------------------%
