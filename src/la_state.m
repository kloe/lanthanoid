%------------------------------------------------------------------------------%

:- module la_state.

%------------------------------------------------------------------------------%

:- interface.
:- use_module la_item.
:- use_module la_map.
:- use_module la_player.
:- use_module list.

	% The game state contains all the state that is necessary for 
	% interaction within the game to be performed. It contains the map,
	% player state, and artifact state.
:- type state.

	% Create a state.
:- pred make(
	la_map.map,
	list.list({la_player.id, {int, int}}),
	list.list({la_player.id, la_item.inventory}),
	state
).
:- mode make(in, in, in, out) is det.

	% Return the map of a state.
:- pred map(state, la_map.map).
:- mode map(in, out) is det.

	% Find the area a player is in, if the player exists.
:- pred player_area(la_player.id, state, {int, int}).
:- mode player_area(in, in, out) is semidet.

	% Find all players in an area.
:- pred area_players({int, int}, state, list.list(la_player.id)).
:- mode area_players(in, in, out) is det.

	% Move a player from one area to another. If the player or the target
	% area does not exist, this predicate will fail.
:- pred relocate_player(la_player.id, {int, int}, state, state).
:- mode relocate_player(in, in, in, out) is semidet.

	% Find the items a player possesses.
:- pred player_inventory(la_player.id, state, la_item.inventory).
:- mode player_inventory(in, in, out) is det.

	% Update the inventory of a player.
:- pred player_inventory(la_player.id, la_item.inventory, state, state).
:- mode player_inventory(in, in, in, out) is det.

%------------------------------------------------------------------------------%

:- implementation.
:- use_module map.
:- use_module multi_map.

:- type state ---> state(
		% The map contains all the areas. It is not to be mutated
		% during gameplay.
	map :: la_map.map,

		% For each player, in which area it is.
	player_areas :: map.map(la_player.id, {int, int}),

		% For each area, the players that are in it.
	area_players :: multi_map.multi_map({int, int}, la_player.id),

		% For each player, which items it possesses.
	player_inventories :: map.map(la_player.id, la_item.inventory)
).

make(Map, PlayerAreasList, PlayerInventoriesList, State) :-
	PlayerAreas = list.foldl(
		func({Player, Area}, Acc) = map.set(Acc, Player, Area),
		PlayerAreasList,
		map.init
	),
	AreaPlayers = list.foldl(
		func({Player, Area}, Acc) = multi_map.set(Acc, Area, Player),
		PlayerAreasList,
		map.init
	),
	PlayerInventories = list.foldl(
		func({Player, Inv}, Acc) = map.set(Acc, Player, Inv),
		PlayerInventoriesList,
		map.init
	),
	State = state(Map, PlayerAreas, AreaPlayers, PlayerInventories).

map(State, Map) :-
	Map = State ^ map.

player_area(Player, State, Area) :-
	map.search(State ^ player_areas, Player, Area).

area_players(Area, State, Players) :-
	( if map.search(State ^ area_players, Area, FoundPlayers)
	then Players = FoundPlayers
	else Players = list.[] ).

relocate_player(P, NewA, !S) :-
	_ = !.S ^ map ^ la_map.area(NewA),
	map.search(!.S ^ player_areas, P, OldA),

	!S ^ player_areas := map.set(!.S ^ player_areas, P, NewA),
	!S ^ area_players := multi_map.delete(!.S ^ area_players, OldA, P),
	!S ^ area_players := multi_map.set(!.S ^ area_players, NewA, P).

player_inventory(Player, State, Inv) :-
	( if map.search(State ^ player_inventories, Player, OkInv)
	then Inv = OkInv
	else la_item.empty(Inv) ).

player_inventory(P, I, !S) :-
	!S ^ player_inventories := map.set(!.S ^ player_inventories, P, I).

%------------------------------------------------------------------------------%
